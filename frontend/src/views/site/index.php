<?php

use backend\models\StaticTextItem;
use yii\helpers\Url;
use backend\models\Page;

/* @var $this yii\web\View */

$this->title = Yii::$app->name;

?>
<div class="category-slider-area">
			<div class="container">
				<div class="row">
<div class="col-md-3">
						<!-- MENU -->
	                    <div class="left-category-menu hidden-sm hidden-xs">
	                        <div class="left-product-cat">
	                            <div class="category-heading">
	                                <h2>Жанры</h2>
	                            </div>
	                            <div class="category-menu-list">
	                                <ul>
	                                    <li><a href="#">Фантастика</a></li>
	                                    <li><a href="#">Детективы</a></li>
	                                    <li><a href="#">Классика</a></li>
										<li><a href="#">Фэнтези</a></li>
	                                    <li><a href="#">Роман</a></li>
	                                    <li><a href="#">Проза</a></li>
										<li><a href="#">Мистика / Ужасы</a></li>
	                                    <li><a href="#">Искусство</a></li>
	                                    <li><a href="#">Драма</a></li>
	                                    <li><a href="#">Вестерн</a></li>
	                                    <li><a href="#">Биография</a></li>
	                                    <li><a href="#">Утопия</a></li>
	                                    <li><a href="#">Антиутопия</a></li>
	                                </ul>
	                            </div>
	                        </div>
	                    </div>
	                    <!-- /MENU -->
						
						<!-- BANNER -->
						
						<? if (!empty($banners)): ?> 
				
<div class="main-slider swiper-container js-main-slider"> 
<div class="main-slider__wrapper swiper-wrapper"> 
<? foreach ($banners as $banner): ?> 
<? /* @var $banner \backend\models\Banner */ ?> 
<div class="slide swiper-slide"> 
<img class="slide__image swiper-lazy" alt="<?= $banner->name ?>" 
data-src="<?= $banner->getSRCPhoto(['suffix' => '_big']) ?>"> 
<div class="slide__content"> 
<div class="slide__cell"> 
<? if (!empty($banner->name)): ?> 
<div class="slide__title"><?= $banner->name ?></div> 
<? endif; ?> 
<? if (!empty($banner->text)): ?> 
<div class="slide__caption"><?= $banner->text ?></div> 
<? endif; ?> 
<? if (!empty($banner->link) || !empty($banner->link_text)): ?> 
<div class="slide__button"> 
<a class="button button_color_dark" 
href="<?= $banner->link ?>"><?= $banner->link_text ?></a> 
</div> 
<? endif; ?> 
</div> 
</div> 
</div> 
<? endforeach; ?> 
</div> 
<div class="main-slider__pagination js-main-slider__pagination"></div> 
<div class="main-slider__button js-main-slider__prev"> 
<svg class="main-slider__icon"> 
<use xlink:href="#icon-arrow-down"></use> 
</svg> 
</div> 
<div class="main-slider__button main-slider__button_next js-main-slider__next"> 
<svg class="main-slider__icon"> 
<use xlink:href="#icon-arrow-down"></use> 
</svg> 
</div> 
</div> 
<? endif; ?>

						<!-- BANNER -->
						
	                </div>
				<div class="col-md-9">	
				<!-- SLIDER -->
						<div class="slider-area">
							<div class="bend niceties preview-1">
								<div id="ensign-nivoslider" class="slides">	
								
									<img src="img/sliders/bg1.jpg" alt="book1" title="#slider-direction-1"/>
				 					<img src="img/sliders/bg2.jpg" alt="book2" title="#slider-direction-2"/>
									<img src="img/sliders/bg3.jpg" alt="book3" title="#slider-direction-3"/>  
								</div>
								<!-- IMAGE  -->
								<div id="slider-direction-1" class="slider-direction">
									<div class="slider-progress"></div>
									<div class="layer-2-1">
										<h1 class="title1">Хорошая книга – бесконечна</h1>
									</div>
								</div>
								<!-- IMAGE 2 -->
								<div id="slider-direction-2" class="slider-direction">
									<div class="slider-progress"></div>
									<!-- layer 1 -->
									<div class="layer-2-1">
										<h1 class="title1">Литература — это самый приятный способ игнорировать жизнь</h1>
									</div>
								</div>
								<!-- IMAGE 3 -->
								<div id="slider-direction-3" class="slider-direction">
									<div class="slider-progress"></div>
									<div class="layer-2-1">
										<h1 class="title1">Книга-это мечта, которую вы держите в руках</h1>
									</div>
								</div>
							</div>
						</div>
						<!-- /SLIDER -->
<? if (!empty($banners)): ?>

    <!-- SLIDER -->
	<div class="slider-area">
		<div class="bend niceties preview-1">
			<div id="ensign-nivoslider" class="slides">	
				<? foreach ($banners as $i => $banner): ?>
					<img src="<?= $banner->getSRCPhoto(['suffix' => '_big']) ?>" alt="book<?= $i?>" title="#slider-direction-<?= $i?>"/>
				<? endforeach; ?>
			</div>
			<!-- IMAGE  -->
			<? foreach ($banners as $i => $banner): ?>
				<div id="slider-direction-<?= $i?>" class="slider-direction">
					<div class="slider-progress"></div>
					<div class="layer-2-1">
						<h1 class="title1"><?= $banner->name ?></h1>
					</div>
				</div>
			<? endforeach; ?>
		</div>
	</div>

<? endif; ?>
<? if (!empty($products)): ?>
    <div class="wrapper">
        <section class="page__section-l">
            <h2 class="title title_centered title_top">Каталог</h2>
			<div class="row">
								
									
            
                
				
													
						<div class="col-xs-12">				
                        <? foreach ($products as $product): ?>
                            <? /* @var $product \backend\models\Product */ ?>
							
                            <div class="col-xs-4">
                                <a href="<?= $product->linkOut ?>" class="card">
                                    <? $src = $index !== null ? $product->getSRCPhoto(['suffix' => '_md', 'index' => $index]) : $product->getSRCPhoto(['suffix' => '_md', 'index' => 0]) ?>
                                    <? if ($product->flags > 0): ?>
                                        <div class="card__badge">
                                            <?= $product->AllFlagsAsArray()[$product->flags] ?>
                                        </div>
                                    <? endif; ?>
                                    <img class="card__image swiper-lazy" src="<?= $src ?>" alt="<?= $product->name ?>">
                                    <div class="card__content">
                                        <span class="card__title"><?= $product->name ?></span>
                                        <div class="card__price"><?= number_format($product->cost, 0, '', ' ') ?>&nbsp;&#8381;</div>
                                    </div>
                                </a>
                            </div>
                        <? endforeach; ?>
						
                    </div>
                    
                
                
                
            
			
        </section>
    </div>
<? endif; ?>
</div>
  </div></div></div>
  

