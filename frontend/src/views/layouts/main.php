<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\helpers\Html;
use backend\widgets\AndNav;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\helpers\Url;
use backend\models\Page;
use backend\models\StaticTextItem;
use backend\models\UserRole;
use backend\models\Base;
use backend\models\PageItem;


AppAsset::register($this);
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <!--<script>//var INLINE_SVG_REVISION = <? //=filemtime(\Yii::getAlias('@frontend/web/svg.html'))?>;</script>-->

        <meta charset="<?= Yii::$app->charset ?>">
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <?= Html::csrfMetaTags() ?>
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		
    </head>
    <body class="body">
    <?php $this->beginBody() ?>

    <?

    $isAbout = \Yii::$app->request->url == $this->params[Page::PAGE_PREFIX . Page::ABOUT_COMPANY]['linkOut'];
    $isContacts = \Yii::$app->request->url == $this->params[Page::PAGE_PREFIX . Page::CONTACTS_PAGE]['linkOut'];
    $isDelivery = \Yii::$app->request->url == $this->params[Page::PAGE_PREFIX . Page::DELIVERY]['linkOut'];
    $isMain = \Yii::$app->controller->id == 'site';
    $isCategory = \Yii::$app->controller->id == 'category';
    $isDesign = \Yii::$app->controller->id == 'design-collection';
    $isReview = \Yii::$app->controller->id == 'review';
    $isError = \Yii::$app->controller->action->id  == 'error';
    //$isProduct = \Yii::$app->controller->id == 'product';
    //$isView = \Yii::$app->controller->action->id == 'view';
    ?>

    <? require_once('_navItems.php'); ?>

    <div class="page" id="vue-app">
        <header class="header">
            <div class="mainmenu-area">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="mainmenu hidden-sm hidden-xs">
								<nav>
									<ul>
										<li><a href="index.php" title="logo"><img src="img/logo.png" height="50" width="50px" alt="logo"></a></li>
										<li><a href="index.php">Главная</a></li>
										<li><a href="contact.php">О нас</a></li>
									</ul>
								</nav>
							</div>
						</div>
					</div>
				</div>
			</div>
        </header>
       
	   
	   <?= $content  ?>
	   
	   
	   
        <footer class="footer">
            <div class="subscribe-area">
				<div class="container">
			
					<div class="row">
						
						<div class="col-md-4 col-sm-5 col-xs-12">
							<div class="social-media">
							Наши контакты:
								<a href="facebook.com"><i class="fa fa-facebook fb"></i></a>
								<a href="vk.com"><i class="fa fa-twitter tt"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>	
        </footer>
        <div id="popup_question" class="popup mfp-with-anim mfp-hide">
            <? require_once('_callback_form.php'); ?>
        </div>
        <?php $this->endBody() ?>

        

    </body>
    </html>
<?php $this->endPage() ?>