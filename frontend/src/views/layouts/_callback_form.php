<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use backend\models\Page;

/* @var $this yii\web\View */
/* @var $form ActiveForm */
?>
<!-- callback_form start-->
<?php \yii\widgets\Pjax::begin() ?>
<?php if ($this->params['message']): ?>
    <div class="title title_size_s title_centered">Спасибо!</div>
    <div class="page__section-xs" style="text-align: center;">
        <?= $this->params['message'] ?>
    </div>
<?php else: ?>
<?php endif; ?>
<?php \yii\widgets\Pjax::end(); ?>
<!-- callback_form -->
